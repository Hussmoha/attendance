package fi.vamk.e2001945.attendancemohamed;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@SpringBootTest
@AutoConfigureMockMvc
public class AttendanceControllerTests extends AbstractTest{
	@Autowired
	private MockMvc mvc;
	@Autowired
	private AttendanceRepository repository;
	
	@WithMockUser("USER")
	@Test
	public void getList() throws Exception{
		Attendance at = new Attendance("first");
		at = repository.save(at);
		Attendance[] arr = new Attendance[1];
		arr[0] = at;
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.get("/attendance/" + at.getId())
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		System.out.println("Gelt list: " + super.mapToJson(at));
		System.out.println(content);
		assertEquals(super.mapToJson(at), content);
	}
	
	@WithMockUser("USER")
	@Test
	public void createAttendanceViaPost() throws Exception {
		Attendance att = new Attendance("ABCDE", "2022-02-01");
	    String inputJson = super.mapToJson(att);
	    att.setId(5);
	    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/attendance")
	      .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
	   
	    int status = mvcResult.getResponse().getStatus();
	    assertEquals(200, status);
	    String content = mvcResult.getResponse().getContentAsString();
	    assertEquals(super.mapToJson(att), content);
	}
	
}
