package fi.vamk.e2001945.attendancemohamed;

import static org.assertj.core.api.Assertions.assertThat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Configuration
@ComponentScan(basePackages = {"fi.vamk.e1901383.semdemo"})
@EnableJpaRepositories(basePackageClasses = AttendanceRepository.class)

public class AttendanceTest {
	@Autowired
	private AttendanceRepository repository;
	
	@Test
	public void postGetDeleteAttendance() throws ParseException {
		/*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date convertedCurrentDate = sdf.parse("2013-09-19");*/
		Attendance att = new Attendance("ABCD", "2013-09-19");
		repository.save(att);
		Attendance found = repository.findByKey(att.getKey());
		assertThat(found.getKey()).isEqualTo(att.getKey());
		repository.delete(att);
	}
	
	@Test
	public void postGetDeleteAttendanceWithDate() throws ParseException {
		/*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date convertedCurrentDate = sdf.parse("2013-09-19");*/
		Attendance att = new Attendance("ABCD", "2013-09-19");
		System.out.println("Att: " + att.toString());
		Attendance saved = repository.save(att);
		System.out.println("Save: " + saved.toString());
		Attendance found = repository.findByDate(att.getDate());
		System.out.println("Found: " + found.getDate());
		//java.util.Date foundDate = new java.util.Date(found.getDate().getTime());
		assertThat(found.getDate()).isEqualTo(att.getDate());
		repository.delete(att);
	}

}
