package fi.vamk.e2001945.attendancemohamed;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name = "Attendance.findAll", query = "Select p From Attendance p")
public class Attendance implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String key;
	private String date;
	
	public Attendance() {
	}
	
	public Attendance(String key) {
		this.key = key;
	}
	
	public Attendance(String key, String date) {
		this.key = key;
		this.date = date;
	}
	
	public Attendance(int id, String key, String date) {
		this.id = id;
		this.key = key;
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Attendance [id=" + id + ", key=" + key + ", date=" + date + "]";
	}	
}
