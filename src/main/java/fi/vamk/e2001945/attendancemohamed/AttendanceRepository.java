package fi.vamk.e2001945.attendancemohamed;

import java.util.Date;

import org.springframework.data.repository.CrudRepository;

public interface AttendanceRepository extends CrudRepository<Attendance, Integer>{
	public Attendance findByKey(String key);
	public Attendance findByDate(String date);
}
