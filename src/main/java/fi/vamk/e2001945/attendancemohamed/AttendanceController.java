package fi.vamk.e2001945.attendancemohamed;

import java.rmi.ServerException;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin(origins = "https://attendace-client.herokuapp.com")
public class AttendanceController {
	@Autowired
	private AttendanceRepository repository;
	
	@GetMapping("/attendances")
	public Iterable<Attendance> list(){
		return repository.findAll();
	}
	
	@GetMapping("/attendance/{id}")
	public Optional<Attendance> get(@PathVariable("id") int id){
		return repository.findById(id);
	}
	
	@PostMapping("/attendance")
	public @ResponseBody Attendance create(@RequestBody Attendance item) throws ServerException {
		if(item.getDate()!=null) {
			return repository.save(item);
		}
		else
			throw new ServerException("Need date");
	}
	
	@PutMapping("/attendance")
	public @ResponseBody Attendance update(@RequestBody Attendance item) throws ServerException {
		if(item.getDate()!=null) {
			return repository.save(item);
		}
		else
			throw new ServerException("Need date");
	}
	
	@DeleteMapping("/attendance")
	public void date(@RequestBody Attendance itme) {
		repository.delete(itme);
	}
	
}
