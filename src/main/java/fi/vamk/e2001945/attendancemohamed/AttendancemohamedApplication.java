package fi.vamk.e2001945.attendancemohamed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class AttendancemohamedApplication { 
    @Autowired
	private AttendanceRepository repository;


	public static void main(String[] args) {
		SpringApplication.run(AttendancemohamedApplication.class, args);
	} 

    @Bean
	public void initDate() throws ParseException {
		/*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date convertedDate1 = sdf.parse("2013-09-18");
		Date convertedDate2 = sdf.parse("2022-03-18");
		Date convertedDate3 = sdf.parse("2021-04-23");*/
		Attendance att1 = new Attendance("first", "2013-09-18");
		Attendance att2 = new Attendance("SECOND", "2021-04-23");
		Attendance att3 = new Attendance("asdAwW", "2022-03-18");
		repository.save(att1);
		repository.save(att2);
		repository.save(att3);
	}


}
